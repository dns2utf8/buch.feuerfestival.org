# Konzepte

Das Festival soll ein Ort zum experimentieren sein.
Neue Konzepte sollen mit der Wirklichkeit kollidieren und Theorien überprüft werden könne.


# Grundsätze

In den sechs Jahren hat sich das erste Konzept entfaltet und wir haben die folgenden Grundsätze extrahiert.


## Radikale Eigenverantwortung

Dies ist ein Mitbring-Festival. Du sorgst selber für dein Essen, deine Unterkunft, dein Wohlergehen und für jenes deiner Mitcamper. Nur Wasser und Toiletten werden zur Verfügung gestellt. Wer das Festivalgelände betritt, handelt auf eigene Gefahr. Der Veranstalter übernimmt keine Haftung. Versicherung ist Sache der Teilnehmer/Innen. 


## Spurlos

Was auch immer du mit auf den Berg bringst, nimmst du auch wieder mit runter.

Jeden Zigarettenstummel, jeden Bierdeckel, jeden Nagel. Auf dem Platz gibt es weder Mülleimer noch eine Entsorgung. Also überlege, was du mitnimmst und wie du es wieder weg schaffst. Bringe Behälter zum Sammeln und entsorge oder recycle dein Material selbständig.

Wir behandeln die Umwelt mit grösstem Respekt. Wir hinterlassen keine Spuren. Ziel ist, den Festivalplatz in einem besseren Zustand zu verlassen, als wir ihn angetroffen haben. 


## Erneuerbar
Das Festival bezieht keine Energie aus der Steckdose oder aus fossilen Brennstoffen und ist eine Experimentierplattform für alternative Energiequellen.

Der Strom für die Grundinfrastruktur wird komplett mit erneuerbaren Energiequellen erzeugt. Auch die Teilnehmer sind aufgerufen, für sich und ihre Projekte selber Strom auf dem Platz zu erzeugen. 


## Radikale Offenheit

Jede und jeder kann ein Teil des Festivals sein. 
Wir heissen das Fremde willkommen und respektieren es. 
Egal welche politische Gesinnung, egal welche sexuelle Ausrichtung, egal welcher Lebenswandel. 
Jede und jeder ist willkommen, der/die diese Offenheit teilt, friedlich ist und die anderen respektiert.


## Radikale Selbstverwirklichung

Sei, was du schon immer sein wolltest. 
Sei das beste Selbst, welches du dir vorstellen kannst. 
Das Festival ist ein Spielfeld deiner Selbst, immer die anderen respektierend. 
Tob dich aus, ignoriere Konventionen. 
Dies ist der Moment, um dich neu zu erfinden. 
Sei es alleine, als Teil eines Projektes in einer Gruppe, verkleidet oder geschminkt, die Möglichkeiten sind unendlich. 


## Selbst gemacht
Dies ist das Fest der Anreisser und Macherinnen, der Spinner und Visionärinnen. Das Festival ist eine Grosskunstwerk. Es ist Freiluftgalerie, Ausstellung und Karneval in einem. Wer den Platz betritt, bringt sich kreativ mit ein. Es gibt keine Zuschauer, nur Teilnehmer. Die Veranstalter stellen nur eine leere Wiese zur Verfügung. Es gibt nichts ausser Trinkwasser, Bau- und Brennholz. Es liegt an den Teilnehmern ihre Bauten und Projekte Tag für Tag wachsen zu lassen und so für wenige Tage eine kuriose neue Welt zu erschaffen.


## Entkommerzialisiert
Am Festival kannst du nichts kaufen oder verkaufen. Nach der Kollekte am Eingang kannst du dein Portemonnaie weg legen. Ob Trinken, Essen, Unterhaltung - was auch immer du haben möchtest: Du musst es selber mitbringen.


## Schenken
Geben ist die Grundlage des Festivals. Schenken ist nicht Tauschen. Erwarte keine Gegenleistung. Ein Geschenk ist an keine Bedingung oder Absicht geknüpft.

Erhälst du ein Geschenk, übernimmst du auch die Verantwortung dafür. (Bierdose, Verpackungen, ...)

Schenkst du, so wirst du reich zurückbeschenkt. Remember, first give - then receive. 

## Brennend

Erster Höhepunkt des Festivals ist das Entzünden des Seelenfeuers – dieses wird während der Eröffnungs-Zeremonie entfacht und während der ganzen Festival-Woche durchgehend am Lodern gehalten. Samstagnacht verwandelt sich der Festplatz in ein brennendes Kunstwerk. Stück für Stück werden brennbare Kunstprojekte vom Seelenfeuer entfacht. Am Sonntagmorgen ist vom Festival nur noch ein Häufchen Asche übrig. 