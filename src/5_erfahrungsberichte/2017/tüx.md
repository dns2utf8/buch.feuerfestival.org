# 2017 Tüx über Vollbrand

Authoren:
* Tüx - Ressorts Webmaster, Musikmeister, Hilfe beim Büro

### Vorbereitungsphase

Der Nachbrand war in diesem Jahr kombiniert mit einer Sitzung.
Das hat sich nicht bewährt.
Zum einen kamen damit unbeteiligte Personen zur Sitzung, welche dann fröhlich über das Festival abstimmten obwohl es sie in keinster Weise betraf.
Zudem wurden die Abstimmungen wegen des steigenden Alkoholpegels sehr unsachlich.

Das führte zu einem Datum an welchem ein Teil des OKs nicht am Festival teilnehmen konnte.

Im Januar fand die erste reguläre Sitzung statt.
An dieser Sitzung wurde nochmals über das Datum diskutiert und dann geändert.
Meiner Meinung nach waren die Hauptgründe dafür:
- Radikale-Inklusion: Alle Personen, auch solche mit Kindern im Kanton Zürich sollten dabei sein.
- Einschluss von mehr Mitgliedern im OK. Mehr Arbeitskraft ermöglicht ein grösseres und interessanteres Festival.

Die Datumsänderung verunsicherte viele.
Trotzdem denke ich, dass es die einzig richtige Entscheidung war.

- Endphase
- Aufbau
- Festival
    - Soundsystem Vagabundenburg, wurde am Mittwoch um 02:35 abgeklemmt.
    - Nachtruhe wurde von Trommelkreisen und Akkordeongruppen nicht eingehalten.
        * Füürsii?
- Brandnacht
    - Betreuung aller Skulpturen nach dem grossen Turm
    - Feedback Gäste, gut dass die grossen Sachen vor 02:00 angezündet worden sind
- Ruhetag
    - Sonntag fiel flach wegen dem drohenden Regen
    - Unklare Kommunikation gegenüber den Helfern, dass sich der Plan geändert hatte
- Abbau
    - Druck vom Regen hilft die Leute zu motivieren
    - Unklare Kommunikation gegenüber den Helfern
