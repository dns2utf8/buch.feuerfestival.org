# Zeltplätze

## Ruhe-/Chilout-Zeltplatz

## Projekt Zeltplatz

## Wald

Der Wald ist ein wunderschöner Ort zum Zelten.

### Gefahren

Bei Sturm oder Böhen fallen jedoch Äste herunter.
Dies führte im Frühling 2017 zu mindestens einem Toten in der Schweiz und mindestens 3 Toten in Deutschland.

### Aufräumen

Nach dem Festival muss der Wald gefötzelt werden.
Das Ziel ist den Wald so zu verlassen wie man ihn vorgefunden hat.

![Waldboden](/Bilder/Waldboden_20170722_153941.jpg)
