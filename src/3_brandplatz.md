# Brandplatz Baupläne

Grundsätzlich müssen auf dem Brandplatz alle Bauten in eigens dafür ausgesteckten Gruben gebaut werden.

Die Gruben sollten etwa 10cm tief sein und das Gras als kompakten Ziegel entfernen.
Die gelagerten Gras-Ziegel können für meherer Tag auf als Haufen gelagert werden.

Nach der Brandnacht kann die Asche in den Gruben bleiben.
Falls Kohle übrig geblieben ist, muss diese entfernt werden.
Wurden bei der Konstruktion Schrauben oder Nägel verwendet muss der Boden mit einem Magnet gereinigt werden.

## Brandnacht

Für die Brandnacht muss genügend Lösch-Kapazität auf dem Platz sein.
In den Jahren 2016 und 2017 hatten wir einen Traktor mit einem Rucksack und 100m Schlauch auf dem Platz.

```
BILD einfügen
````

Der Rucksack hatte eine Kapazität von 750 Liter, gefüllt mit Wasser.
Mit der Pumpe erreicht der Traktor einen Druck von etwa 40bar.

Die handliche Spritze am Ende des Schlauches erlaubt es präzise zu löschen.