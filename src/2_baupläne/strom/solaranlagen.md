# Solaranlagen

## Strom

## Laderegler

Laderegler brauchen auch etwas Strom.
Bei vielen Modellen ist es üblich, dass sie bei einer Störung oder bei **kompletter Entladung** sehr laut pfeifen.

Erfahrungsgemäss haben schlafende Personen in unmittelbarer Nähe dann wenig Geduld das Problem zu beheben.
Ein einfach zu bedienender Schalter, welcher die Last trennt und damit die weitere Entladung verhindert ist das einfachste Mittel.
Dann werden die Zellen bei Sonnenaufgang wieder geladen während die Menschen schlafen können.

## Wärme

Grundsätzlich ist ein schwarzer Schlauch mit Wasser drinn auch eine Solaranlage.
Bisher hat jedoch noch keine eine Dusche mit warmem Wasser gebaut.
