# Köhlergrube

```
Dieses Kapitel sollte von Christian Marty überarbeitet und erweitert werden.
```

Beim Köhlern wird Holz so heiss gemacht, dass dabei ein [Pyrolyse-Gas](https://de.wikipedia.org/wiki/Biomassevergasung) ensteht, welches ganz tief unter bleibt und dort verbrennt.
Dieses weisse Gas, welches nicht aufsteigt, macht die eigentliche Kohle.

<video alt="Video Pyrolyse-Gas" controls src="/2_baupläne/köhlergrube/VID_20170724_135241.mp4"></video>

## Vorbereitungen
Im Jahr 2017 brauchten wir etwa einen halben Anhänger Holzschnitzel.
![Schnitzelvorrat](/2_baupläne/köhlergrube/Schnitzelvorrat_20170724_162908.jpg)

Um den unkontrollierten Wasserfluss zu bändigen haben wir einen Wassergraben gemacht.
Dieser erlaubt es auch nach dem Köhlern geziehlt Wasser in die Grube umzuleiten.

![](/2_baupläne/köhlergrube/IMG_20170724_114247.jpg)
![](/2_baupläne/köhlergrube/IMG_20170724_114258.jpg)
![](/2_baupläne/köhlergrube/IMG_20170724_114302.jpg)
![](/2_baupläne/köhlergrube/IMG_20170724_114342.jpg)

Um den Kot etwas abzutropfen haben wir ihn auf einer schrägen Blache zwischengelagert.

![Kotlager](/2_baupläne/köhlergrube/Kotlager_20170724_162723.jpg)

## Köhlern

Zuerst mischt man die Holzschnitzel mit dem Kot/Schnitzel-Gemisch.
Gute Erfahrung machten wir mit einer Mischung, welche noch etwas feucht war/aussah.

![Kot-/Schnitzel-Mischung](/2_baupläne/köhlergrube/Mischung_20170724_162730.jpg)

![](/2_baupläne/köhlergrube/IMG_20170724_135217.jpg)

Sobald der Kot auf dem Feuer liegt kommt sehr viel Dampf zum Vorschein.
![](/2_baupläne/köhlergrube/Dampf_20170724_211031.jpg)

## Letzter Brand
Nachdem der ganze Kot in der Grube ist, muss nochmal ein grosses Feuer gemacht werden.
Es soll die oberste Schicht Kot sterilisieren und zu Kohle verwandeln.
![](/2_baupläne/köhlergrube/Durchbrand_20170724_162755.jpg)

Wenn das Feuer zu einer Glut heruntergebrannt ist wird die ganze Grube mit Wasser gelöscht.

![](/2_baupläne/köhlergrube/IMG_20170724_210004.jpg)

## Abschöpfen am nächsten Tag

Am Anfang schwimmt ein grossteil der Kohle oben auf.
Dies sind aber nur die grösseren Stücke.
Am Boden liegen die feineren Kohlestücke.

![](/2_baupläne/köhlergrube/IMG_20170725_122236.jpg)
![](/2_baupläne/köhlergrube/IMG_20170725_121513.jpg)

Das Entwässern geht mit einem Fass und einer Schaufel als Filter sehr gut.
Zu dritt konnten wir die halbe Grube in etwas mehr als einer Stunde entleeren und die Kohle vom Grund wegschaufeln.

![](/2_baupläne/köhlergrube/IMG_20170725_122819.jpg)
![](/2_baupläne/köhlergrube/IMG_20170725_123640.jpg)

## Das Resultat
![](/2_baupläne/köhlergrube/IMG_20170725_135332.jpg)
![](/2_baupläne/köhlergrube/IMG_20170725_135509.jpg)

## Nach dem Zuschütten
![](/2_baupläne/köhlergrube/IMG_20170725_135747.jpg)
