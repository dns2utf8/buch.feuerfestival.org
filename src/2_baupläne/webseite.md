# Webseite

Die IT hat folgende aufgaben:
* Registrieren der Domänen (der Name im Internet)
* Verwaltung Webspace
    * Seit 2011 bei metanet.ch
    * Bezahlen der Rechnung
    * Koordination bei Hacks/Viren
* Betrieb und Administration eMail Weiterleitungen
* Betrieb und Administration eMail Mailinglisten
* Betrieb der Webseiten
    * Unterstützung des Büros mit den Texten
    * Kontrolliern aller Links
    * Koordinieren der Übersetzungen
* Archivierung der Fotos
* Galerie betreuen
    * Fotos aussortieren
    * Videos verlinken

Es wurden auch schon Foren oder Soziale-Netzwerke gewünscht.
Diese übersteigen aber bis heute die personellen Möglichkeiten der OKs.

## Homepages

Die folgenden Domänen werden im Moment gehalten:

* feuerfestival.org (2011-heute)
* liechterloo.org (2012-2017)
* mondgluet.org (2013-2017)
* füürwärts.org (2014-heute)
* sunnebrand.org (2015-heute)
* vollbrand.org (2016-heute)
* illuminox.ch (2017-heute)

## Übersetzungen

Die Homepages der Festival wurden seit der ersten Durchführung in 2 (Deutsch und Englisch für feuerfestival.org) bzw. 3 (Deutsch, Englisch und Französisch) Sprachen übersetzt.

## Mailing Listen

Interne Kommunikation

## Event Tool 2017

War 2017 ein Projekt von Tüx um die Selbstorganisation der Teilnehmer zu ermöglichen.
Das Projekt wurde an der HSR.ch durchgeführt brachte aber keine direkt verwertbaren Ergebnisse.

## Git (Repo)

Das Versionskontrollsystem Git wurde mit der ersten selbst-entwickelten Homepage für Mondgluet 2013 eingeführt.
Von 2013 bis 2017 hat Tüx das Hosting selber gemacht.

Anfang 2017 wuchs das Web Team noch weiter und das verwalten der SSH Schlüssel wurde zu aufwendig.
Darum wurden dann der Wechsel des Hostings zu [Gitlab.com](https://gitlab.com/FeuerFestival) durchgeführt.

### Automatisches Veröffentlichen mit Continous Integration (CI)

Da alle Informationen und Daten für die Homepage in Git gespeichert sind kann die Webseite von einem Runner aus dem Repo gebaut werden.
Nachdem die Webseite auf dem Runner gebaut wurde kann sie in einem nächsten Schritt auf den Webserver übertragen werden.

Auszug aus `.gitlab-ci.yml`:

```yaml
deploy:book:
  script:
  - apt update && apt install -y lftp
  - lftp -c "open -u $FTP_USER,$FTP_PASS buch.feuerfestival.org; set ssl:verify-certificate no; mirror -R book ."
```

Das Skript benötigt die geheime Variabeln `FTP_USER` und `FTP_PASS`, welche auf Gitlab bei jedem Repo induviduell unter "Settings" -> "CI /CD" -> "Secret variables" hinterlegt werden müssen.

Beim den Variabeln muss darauf geachtet werden dass nur die Zeichen `A-Z`, `a-z`, `0-9` und `_` verwendet werden, da sonst nicht mehr klar ist was nocht zur Variabel gehöhrt und was nicht.
