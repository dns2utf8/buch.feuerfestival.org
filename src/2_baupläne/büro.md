# Büro

```
Diese Kapitel sollte von Ändu und Luzi ergänzt werden.
```

Grundsätzlich gilt das Büro koordiniert alles was sonst liegenbleiben würde.
Je mehr delegiert wird desto mehr Kapazität wird frei.

Unabdingbare Aufgaben:
- Führen der Adressliste
- Regelmässiges Einberufen der grossen Sitzungen
- Bewilligung einhohlen
- Verfassen des Protokolls der Sitzung und den Versand überprüfen
- Kontakt mit Ressor Leiter/-innen
  - Merken wann jemand aufhört
  - Merken ob zusätzliche Helfer benötigt werden
- Antworten auf kontakt@NAME.org
- Halbjährlicher Newsletter verschicken

Optionale oder delegierbare Aufgaben:
- Kleinkust suchen und organisieren
- Shuttle Organisieren
- Kartenmaterial erstellen / pflegen
