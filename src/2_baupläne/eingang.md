# Eingang

```
Dieses Kapitel sollte noch von Leuten ergäntzt werden, welche den Eingang bereits organisiert haben.
```

## Funktionen Sicht OK

- Verteilen von Informationen
- Sammeln der Kollekte
- Sammeln von Adressen von interessierten Personen für das nächste Fest
- Sammeln von neuen potentiellen Mitgliedern für das OK

## Funktionen Sicht Gäste

Der Eingang ist die zentrale Anlaufstelle für Informationen.

Es wurden schon als Auffrischung gedachte kurze Zusammenfassungen der Informationen der Homepage verteilt.
Das scheint vor allem bei den Party-Gästen am Wochenende wirksam zu sein.

Workshops und Konzerte können dort auch auf Tafeln angeschrieben werden.
Diese Funktion hat allerdings einen begrenzten Einfluss, wenn der Eingang abgesetzt ist.
