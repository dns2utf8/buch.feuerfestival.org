# Funk & WLan

## Grosse Distanzen Planen LTE

Zuerst auf der Karte von [Swisstopo](https://map.geo.admin.ch/?lang=de&topic=funksender&bgLayer=ch.swisstopo.pixelkarte-farbe&layers=ch.bakom.radio-fernsehsender,ch.bakom.mobil-antennenstandorte-gsm,ch.bakom.mobil-antennenstandorte-umts,ch.bakom.mobil-antennenstandorte-lte&E=2614202.95&N=1192120.76&zoom=7&catalogNodes=403,408&layers_visibility=false,false,true,true) die 3G und 4G Türme einblenden:
Erweiterte Werkzeuge -> Funksender -> Antennenstandort

Dann eine Linie vom Platz zum nächsten Trum ziehen und das Gelände mit [airLink](https://airlink.ubnt.com/#/ptp) überprüfen.

Gute Werte für die Antenne sind 10m über Boden.

Der Client ist üblicherweise 2-3m über dem Boden.
