# CareTeam / Kollektiv

Das CareTeam hilft dem OK mit den Massen umzugehen.
Inzwischen ist das Team extrem gut organisiert und ist vollständig in das Sicherheitskonzept eingebunden.

## Vorgehen

Auf dem Platz wechseln sie sich mit einem Schitplan ab.

Während den Schichten sind die Mitglieder alle nüchtern.

In der nacht ist zusätzlich zu den Personen auf Patrulie noch jemand auf Abruf bereit.


## Aufgaben

* Wetter beobachten
* Ewakuierung
  * Teil-Ewakuierung: bei Sturm Böhen den Wald leeren
  * Voll-Ewakuierung: bei Gewitter oder Feuern ausser Kontrolle
* Nachtruhe
* Meldung von psychisch auffälligen Personen an Sanität

## Kompetenzen

* Ausrufen der Evakuation
