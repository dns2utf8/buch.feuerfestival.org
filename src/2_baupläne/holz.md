# Holz

```
Dieses Kapitel sollte von Christian Baumgartner unserem Holz-Meister überarbeitet und erweitert werden.
```

## 2014 - 2015

Im April 30 bis 40 Ster Holz im Wald neben dem Festivalgelände eingelagert.

## 2016 - 2017

50 Ster Holz vor dem Festival bestellt.

Mit Möglichkeit während dem Festival nochmals 10 bis 20 Ster nach zu bestellen.

Das Holz wird direkt von einem Sägewerk mit einem Holztransporter geliefert.
