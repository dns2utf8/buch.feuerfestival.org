# Unterstand

Für die Besucher ist es wichtig einen gemeinsamen Platz für allerlei Austausch zu haben.

Bei Sonne muss der Unterstand genügend Schatten bieten um kleine Workshops und Kinder aufzunehmen.

Bei Regen muss der Unterstand einen grossen Teil der Leute auf dem Brandplatz aufnehmen können.
Also bei z.B. 1000 Besuchern unter der Woche sollten sich bei einem Regenguss etwa 500 Personen unterstellen können.

## Nebennutzung Bühne

In den Jahren 2011 bis 2017 war der Unterstand immer das grösste Zelt.
Damit kam auch die Bühne in das Zelt.

Leider betrachten manche Besucher das allgemeine Zelt als persönlichen Wohn- oder Lagerraum.
Das ist gefährlich wenn bei einem spontanten Unwetter viele Menschen in den Raum flüchten.
Es verstopft zudem dem Raum für andere Aktivitäten wir Workshops oder Konzerte.
