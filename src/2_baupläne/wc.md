# WC

```
Dieses Kapitel sollte noch stark erweitert und überarbeitet werden.
```

## Plumpsklo

### Aufbau auf einem Bschüttfass

TODO Schätzen für wie viele Personen

Das Design ist sehr flexibel. 
Das Gerüst wurde oft von Besuchern ohne weitere Hilfe aufgebaut.

Für das Grundgerüst eignen sich z.B. Wasserrohre mit diversen Verbindungsstücken.
Ein Vorrat davon wird in der Mühle Schangnau aufbewahrt.

![Bschüttfass Plumpsklo Vollbrand](/2_baupläne/wc/Bschüttfass-Plumpsklo_20170717_194000.jpg)

### Plumsklo mit Trennvorrichtung und Sickergrube

Für ca. 50-70 Personen an 7 Tagen.

Bild von Moves Festival 2017

![Plumpsklo Moves Festival 2017](/2_baupläne/wc/Mini_Plumpsklo_IMG_20170819_113425.jpg)


## WC-Wagen mit Wasserspühlung

TODO Bild vom vergrabenen Rad.


## Kompotoi

2016 buchten wir für 3600.- die Dienstleistung von Kompotoi.

Während der Aufbauphase wurden vier frei-stehende Klos geliefert.
Die Klos hatten Urintrenner und ein integriertes Urinal.
Die Struktur war aus hochwertigem Holz gebaut und hatten eine abschliessbare Tür.

Am Montag wurde dann eine Batterie mit 6 oder 7 Sitzplätzen geliefert.
Die Konstruktion hatte keine Urintrenner.
Der Urin wurde am Boden des Plumsklos von einem Gemisch aus Holzspänen und Stroh durchgelassen während der Kot darauf liegen blieb.
Der abtropfende Urin sammelte sich so am Boden und musste abgepumpt werden.

Das Problem ist dass das Abpumpen Strom benötigt.
Am Feuerfestival haben wir das mit einem Akku-Roller gemacht.
Das bedeutete als die Besucherzahl zunahm musste das Abpumpen häufiger gemacht werden.
Als die Frequenz am Freitag nicht erhöht wurde lief allerdings eine grössere Menge Urin aus und verpestete den Brandplatz.


## Eigenbau Holzklo mit Urin-Trenner

![Eigenbau WC beim Transport](/2_baupläne/wc/Eigenbau_20170725_141332.jpg)


## Klimacamp Klo mit Urin-Trenner

Ein Konzept mit 5 Sitzplätzen und 4 Trichtern für ein Urinal.

Die Sitzplätze haben Urin-Trenner installiert.

Aller Urin wird in einem Metalltank gesammelt.
Am Vollbrand 2017 hatte der Tank eine Kapazität von ca 2000l.
