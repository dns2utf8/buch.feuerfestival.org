# Ei

Das Feuerei entfaltet seine faszinierende Wirkung durch den Sog des Feuers.

```
         ^
         |
         |  10 bis 15 Meter Stichflamme
         |
         |
        _ _ 
       (   )
      (  ^  )
     (   |   )
      (     )
       (   )
        | |
        | |
   -->  | | <-- Luft
---------------------
```

Bei der Konstruktion des Eis muss darauf geachtet werden, dass die Luft unten in der Mitte angesogen werden kann.

Sobald das Zündholz im Innern verbrandt ist, muss der Deckel oben in der Mitte nachgeben um den Luftfluss nach oben zu ermöglichen.