# Turm

```
Dieses Kapitel sollte von Christian Marty ergänzt und überarbeite werden.
```

Ein Turm auf dem Brandplatz überragt fast alles.
Er muss auf jeden Fall abgespannt werden.

## Als Aussichtsplattform

Damit der Turm viele Leute sicher beherbergen kann müssen die tragenden Pfeiler mindestens 0.5 Meter in den Boden vergraben werden.

Die Leiter für den Aufstieg sollte entweder entfernbar, abschliessbar oder Kindersicher gemacht werden.
Sonst müsste der Turm die ganze Zeit beaufsichtigt werden.

## Als Feuerskulptur

Die tragenden Pfosten sollten entweder umreissbar sein oder aber nicht so tief vergraben werden.
So dass sie beim Abbrennen umfallen können.
Dies entspannt die Aufsehenden Personen, weil sie sobald alles liegt sich wieder auf die nächste Skulptur konzentrieren können.
