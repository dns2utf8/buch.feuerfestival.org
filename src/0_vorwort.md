# Vorwort

Dieses Handbuch soll OK-Mitgliedern und interessierten Menschen einen Einblick in das Werken hinter dem Feuerfestival geben. 
Das Konzept des Feuerfestivals und die bereits gesammelten Erkenntnisse zu sammeln kam in der Vorbereitungsphase zum Vollbrand 2018 auf um neuen Mitgliedern den Einstieg in die Organisation hinter dem Festival zu erleichtern und das Entwickeln und Übergeben von Ressort zu vereinfachen.

Dieses Projekt wird aktuell von Tüx betreut.
Mitarbeit ist sehr erwünscht.
Bitte melde dich ungeniert bei [Tüx](mailto:tuex@estada.ch).

# Mitarbeit

Rückmeldungen, Anmerkungen oder Korrekturen können entweder direkt auf [Gitlab](https://gitlab.com/dns2utf8/buch.feuerfestival.org) oder per eMail an [Tüx](mailto:tuex@estada.ch) gehen.

## Dokumentation

Dieses Projekt wurde mit mdbook erstellt.
Die Dokumentation finden Sie hier: [https://github.com/rust-lang-nursery/mdBook](https://github.com/rust-lang-nursery/mdBook)
