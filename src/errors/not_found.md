# 404 not found

Die gewünschte Seite konnte nicht gefunden werden.

Bitte wähle ein Kapitel aus der Übersicht links oder gehe [zum Anfang](/)

<script>
var base = document.querySelector("head base");

if (!!base) {
  var len = location.pathname.split('/').length - 1;
  base = (p = new Array(len)).fill('..', 0, len).join('/') + '/';

  [{ e: 'a', f: 'href' }, { e: 'link', f: 'href' }, { e: 'script', f: 'src' }].forEach(function(o) {
    var a = document.querySelectorAll(o.e)

    a.forEach(function(e) {
      var path = e.getAttribute(o.f);
      if (!path) { console.log(['skip', e, path, o]); return 7; }
      if (path.indexOf('http') !== 0 && path.indexOf('://') !== 0) {
        e[o.f] = base + path;
      }
    });
  });

}

$("html").addClass("sidebar-visible")

</script>
