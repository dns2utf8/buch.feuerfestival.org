# Buch über das Feuerfestival der Schweiz

Um am Buch mit zuarbeiten können direkt die Text-Dateien im Ordner `src` bearbeitet werden.

Die aktuell veröffentlichte Version finden Sie unter https://buch.feuerfestival.org

# Lokal testen

Das Projekt verwendet `mdbook`, welches in der Programmiersprache [rust](https://www.rust-lang.org/) geschrieben ist.

Der einfachste Weg ist nach der Installation von [rustup.rs](https://rustup.rs/) ist die Installaiton mit folgendem Befehl:

```
cargo install -f mdbook
```

Dann das Buch mit dem Befehl starten:
```
mdbook serve
```

Zum Schluss mit dem Browser die [lokale Version des Buches öffnen](http://localhost:3000).
